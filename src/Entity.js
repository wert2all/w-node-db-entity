'use strict';
const WGData = require('w-node-data-value');

/**
 * @class EMEntity
 * @type EMEntity
 */
class EMEntity {

    /**
     *
     * @param {DBModelInterface} model
     * @param {{}} data
     */
    constructor(model, data) {
        /**
         * @type {DBModelInterface}
         * @private
         */
        this._model = model;
        /**
         *
         * @type {WGDataInterface}
         * @private
         */
        this._data = new WGData(data);
        /**
         *
         * @type {[EMEntity]}
         * @private
         */
        this._children = [];
    }

    /**
     *
     * @param {string} field
     * @param {*} value
     * @returns {EMEntity}
     */
    setValue(field, value) {
        this._data = this._data.setData(field, value);
        return this;
    }

    /**
     *
     * @returns {WGDataInterface}
     */
    getData() {
        return this._data;
    }

    /**
     *
     * @returns {DBModelInterface}
     */
    getModel() {
        return this._model;
    }

    /**
     *
     * @param {EMEntity} entity
     * @returns {EMEntity}
     */
    addChild(entity) {
        this._children.push(entity);
        return this;
    }

    /**
     *
     * @return {EMEntity[]}
     */
    getChildren() {
        return this._children;
    }
}

module.exports = EMEntity;
