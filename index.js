'use strict';

/**
 * The entry point.
 *
 * @module EMEntity
 */
module.exports = require('./src/Entity');
